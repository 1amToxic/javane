import java.util.Scanner;

public class DSA06023 {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int n = sc.nextInt();
        int[] a = new int[n+5];
        for(int i = 1; i <= n; i++){
            a[i] = sc.nextInt();
        }
        int tmp;
        for(int i = 1; i < n; i++){
            for(int j = i+1; j <= n; j++){
                if(a[i] > a[j]){
                    tmp = a[i];
                    a[i] = a[j];
                    a[j] = tmp;
                }
            }
            System.out.print("Buoc "+i+": ");
            for(int k = 1; k <= n; k++)
                System.out.print(a[k]+" ");
            System.out.println();
        }
    }
}
