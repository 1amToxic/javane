import java.util.*;

public class J02025 {
    static int[] x;
    static int[] a;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int test = sc.nextInt();
        while (test-- > 0) {
            int n = sc.nextInt();
            a = new int[n + 5];
            x = new int[n + 5];
            int[] c = new int[n + 5];
            for (int i = 1; i <= n; i++)
                c[i] = sc.nextInt();
            Arrays.sort(c, 1, n + 1);
            for (int i = 1; i <= n; i++) a[i] = c[n - i + 1];
            Try(1, n);
        }
    }

    static void Try(int k, int n) {
        for (int i = 0; i <= 1; i++) {
            x[k] = i;
            if (k == n) {
                check(n);
            } else Try(k + 1, n);
        }
    }

    private static void check(int n) {
        int sum = 0;
        for (int i = 1; i <= n; i++)
            if (x[i] == 1)
                sum += a[i];
        if (checkPrime(sum)) {
            for (int i = 1; i <= n; i++)
                if (x[i] == 1) {
                    System.out.print(a[i] + " ");
                }
            System.out.println();
        }
    }

    static boolean checkPrime(int n) {
        if (n < 2) return false;
        else for (int i = 2; i <= Math.sqrt(n); i++)
            if (n % i == 0) return false;
        return true;
    }
}
