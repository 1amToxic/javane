import java.math.BigInteger;
import java.util.Scanner;

public class T205 {
    static Scanner sc = new Scanner(System.in);
    static int test;
    static BigInteger n, k;
    static int mod = 1000000007;

    public static void main(String[] args) {
        test = sc.nextInt();
        while (test-- > 0) {
            n = sc.nextBigInteger();
            k = sc.nextBigInteger();
            System.out.println(n.modPow(k, BigInteger.valueOf(mod)).longValue());
        }
    }

    private static int dequy(int n, int k) {
        if (k == 1) return n;
        if (k == 0) return 1;
        if (k % 2 == 0) {
            int x = dequy(n, k / 2) % mod;
            return x * x;
        } else return dequy(n, k - 1) * n;
    }
}
