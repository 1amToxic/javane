import java.util.Scanner;

public class a {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n= sc.nextInt();
        for(int i = 1; i <= n; i++){
            int m = sc.nextInt();
            int arr[] = new int[m+5];
            for(int j = 1; j <= m; j++){
               arr[j] = sc.nextInt();
            }
            boolean dx = true;
            for(int j = 1; j <= m/2; j++){
                if(arr[j] != arr[m-j+1]){
                    dx = false;
                    break;
                }
            }
            if(dx) System.out.println("YES");
            else System.out.println("NO");
        }
    }
}
