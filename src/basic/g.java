import java.util.Arrays;
import java.util.Scanner;

public class g {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        sc.nextLine();
        for(int i = 1; i <= n; i++){
            String s = sc.nextLine();
            char[] arrStr = s.toCharArray();
            Arrays.sort(arrStr);
            int sum = 0;
            for(int j = 0; j < arrStr.length; j++){
                if(Character.isDigit(arrStr[j])){
                    sum += Integer.parseInt(String.valueOf(arrStr[j]));
                }else{
                    System.out.print(arrStr[j]);
                }
            }
            System.out.print(sum);
            System.out.println();
        }
    }
}
