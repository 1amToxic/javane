import java.util.Scanner;

public class t305 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int test = sc.nextInt();
        while(test-->0) {
            int n = sc.nextInt();
            int[] a = new int[n + 5];
            for (int i = 1; i <= n; i++) a[i] = sc.nextInt();
            qhd(a, n);
        }
    }

    private static void qhd(int[] a, int n) {
        int kq = 0;
        int[] f = new int[n+5];
        for(int i = 1; i <= n; i++){
            f[i] = a[i];
            for(int j = 1; j < i; j++){
                if(a[i] > a[j])
                    f[i] = Math.max(f[i],f[j]+a[i]);
            }
            kq = Math.max(f[i],kq);
        }
        System.out.println(kq);
    }
}
