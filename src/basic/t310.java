import java.util.Scanner;

public class t310 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        while (t-- > 0) {
            String s = sc.nextLine();
            System.out.println(qhd(s));
        }
    }

    private static int qhd(String s) {
        int kq = 1;
        if (s.length() == 1) return 1;
        int[][] f = new int[s.length() + 5][s.length() + 5];
        for (int i = 0; i < s.length(); i++)
            for (int j = 0; j < s.length(); j++)
                f[i][j] = 1;
        for (int i = 1; i < s.length(); i++) {
            for (int j = 0; j < i; j++) {
                if (s.charAt(i) == s.charAt(j)) {
                    if (i == j + 1) {
                        f[i][j] = 2;
                    } else if (f[i - 1][j + 1] == Math.abs(i - j - 2) + 1) {
                        f[i][j] = Math.max(f[i - 1][j + 1] + 2, f[i][j]);
                    }
                }
                kq = Math.max(f[i][j], kq);
            }
        }
        return kq;
    }
}
