import java.util.Scanner;

public class t314 {
    static int f[][];

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while (t-- > 0) {
            int n = sc.nextInt();
            int v = sc.nextInt();
            int[] a = new int[n + 5];
            int[] c = new int[n + 5];
            for (int i = 1; i <= n; i++) a[i] = sc.nextInt();
            for (int i = 1; i <= n; i++) c[i] = sc.nextInt();
            System.out.println(qhd(a, c, n, v));
        }
    }

    private static int qhd(int[] a, int[] c, int n, int v) {
        f = new int[n + 5][v + 5];
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= v; j++) {
                f[i][j] = f[i - 1][j];
                if (j >= a[i]) f[i][j] = Math.max(f[i][j],f[i-1][j-a[i]]+c[i]);
            }
        }
        return f[n][v];
    }
}
