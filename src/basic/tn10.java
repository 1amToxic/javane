import java.util.Scanner;

public class tn10 {
    static int[] a = new int[15];
    static boolean[] status = new boolean[15];
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        for (int i = 1; i <= n; i++) {
            int m = sc.nextInt();
            for(int k = 1; k <= m; k++)
                status[k] = false;
            Try(1,m);
            System.out.println();
        }
    }
    static void Try(int k, int n){
        for(int i = n; i >= 1; i--){
            if(!status[i]){
                a[k] = i;
                status[i] = true;
                if(k == n)  print(n);
                else Try(k+1,n);
                status[i] = false;
            }
        }
    }
    static void print(int n){
        for(int i = 1; i <= n; i++)
            System.out.print(a[i]);
        System.out.print(" ");
    }
}
