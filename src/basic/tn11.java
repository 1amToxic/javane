import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class tn11 {
    static int[] a = new int[20];
    static List<String> list = new ArrayList<>();
    static String s;
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int test = sc.nextInt();
        while(test-- > 0){
            list.clear();
            int n = sc.nextInt();
            sc.nextLine();
            s = sc.nextLine();
            Try(1,n);
            Collections.sort(list);
            for(String s : list)
                System.out.print(s+" ");
            System.out.println();
        }
    }

    private static void Try(int k, int n) {
        for(int i = 0; i <= 1; i++){
            a[k] = i;
            if(k == n) {
                addToList(n);
            }else{
                Try(k+1,n);
            }
        }
    }

    private static void addToList(int n) {
        StringBuilder t = new StringBuilder("");
        for(int i = 1; i <= n; i++){
            if(a[i] == 1){
                t.append(s.charAt(i-1));
            }
        }
        if(!t.toString().isEmpty())
            list.add(t.toString());
    }
}
