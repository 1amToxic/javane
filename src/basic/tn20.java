import java.util.Scanner;

public class tn20 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int test = sc.nextInt();
        while (test-- > 0){
            int a = sc.nextInt();
            int b = sc.nextInt();
            if(b%a==0){
                b = b/a;
                System.out.println("1/"+b);
            }else{
                int bcnn = a*b/gcd(a,b);
                a = a * (bcnn/b);
                b = bcnn;
                int i = a;
                while(a > 0 && i > 0){
                    if(b%i==0 && a >= i){
                        System.out.print(1+"/"+b/i);
                        a-=i;
                        if(a > 0){
                            System.out.print(" + ");
                        }
                    }
                    i--;
                }
                if(a>0){
                    for(i = 1; i <= a; i++){
                        System.out.print(1+"/"+b);
                        if(i != a) System.out.print(" + ");
                    }

                }
                System.out.println();
            }

        }
    }
    static int gcd(int a, int b){
        if(b==0)    return a;
        return gcd(b,a%b);
    }
}
