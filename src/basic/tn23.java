import java.util.Arrays;
import java.util.Scanner;

public class tn23 {
    static int temp;
    static int[] t, arr;
    static int n, S;
    static boolean isStop;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int test = sc.nextInt();
        while (test-- > 0) {
            n = sc.nextInt();
            S = sc.nextInt();
            t = new int[n + 5];
            arr = new int[n + 5];
            temp = 0;
            isStop = false;
            for (int i = 1; i <= n; i++)
                t[i] = sc.nextInt();
            Arrays.sort(t, 1, n + 1);
            Try(n);
            if(temp!=S){
                System.out.println(-1);
            }
        }
    }

    static void Try(int i) {
        if (i == 0)
            return;
        if (!isStop) {
            for(int j = 1; j >=0; j--){
                arr[i] = j;
                if(!isStop){
                    temp += arr[i] * t[i];
                    if(temp == S && i > -1){
                        result();
                        isStop = true;
                    }else if(temp < S){
                        Try(i-1);
                    }
                }
                if(!isStop)
                    temp -= arr[i] * t[i];
            }
        }
    }

    private static void result() {
        int count = 0;
        for(int i = 1; i <= n; i++)
            if(arr[i] == 1)
                count++;
        System.out.println(count);
    }
}
