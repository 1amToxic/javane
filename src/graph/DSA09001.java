package graph;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DSA09001 {
    static List<Integer>[] ke = new ArrayList[1005];
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(System.in);
        int test = scanner.nextInt();
        while (test-->0){
            int v = scanner.nextInt(), e = scanner.nextInt();
            for(int i = 1; i <= v; i++){
                ke[i] = new ArrayList<>();
            }
            for(int i = 1; i <= e; i++){
                int a = scanner.nextInt(),b = scanner.nextInt();
                ke[a].add(b);
                ke[b].add(a);
            }
            for(int i = 1; i <= v; i++){
                System.out.print(i+": ");
                for(int j = 0; j < ke[i].size(); j++)
                    System.out.print(ke[i].get(j)+" ");
                System.out.println();
            }
        }
    }
}
