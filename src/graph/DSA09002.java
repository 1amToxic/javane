package graph;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

public class DSA09002 {
    static List<Integer>[] a = new ArrayList[1003];
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());
        for(int i = 1; i <= n; i++){
            a[i] = new ArrayList<>();
            String s = scanner.nextLine();
            StringTokenizer stk = new StringTokenizer(s," ");
            while (stk.hasMoreTokens()){
                int k = Integer.parseInt(stk.nextToken());
                if(i < k)
                    a[i].add(k);
            }
        }
        for(int i = 1; i <= n; i++){
            if(a[i].size() > 0){
                for(int j = 0 ; j < a[i].size(); j++)
                    System.out.println(i +" "+a[i].get(j));
            }
        }
    }
}
