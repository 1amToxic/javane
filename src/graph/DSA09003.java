package graph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class DSA09003 {
    static ArrayList<Integer>[] lists = new ArrayList[1003];

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int test = scanner.nextInt();
        while (test-->0){
            int V = scanner.nextInt(),E = scanner.nextInt();
            for(int i =0; i <= V;i ++){
                lists[i] = new ArrayList<>();

            }
            for(int i = 1; i <= E; i++){
                int x= scanner.nextInt(),y =scanner.nextInt();
                lists[x].add(y);
            }
            for(int i = 1; i <= V;i++){
                System.out.print(i+": ");
                Collections.sort(lists[i]);
                for(Integer k : lists[i]){
                    System.out.print(k+" ");
                }
                System.out.println();
            }
        }
    }
}
