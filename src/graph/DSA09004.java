package graph;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

public class DSA09004 {
    static List<Integer>[] a = new ArrayList[1003];
    static int[] dd = new int[1003];
    static Stack<Integer> stack = new Stack<>();
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(System.in);
        int test = scanner.nextInt();
        while (test-- > 0) {
            int V = scanner.nextInt(), E = scanner.nextInt(), u = scanner.nextInt();
            for (int i = 0; i <= V; i++) {
                a[i] = new ArrayList<>();
                dd[i] = 0;
            }
            for (int i = 1; i <= E; i++) {
                int x = scanner.nextInt(), y = scanner.nextInt();
                a[x].add(y);
                a[y].add(x);
            }
            dfs(u);
            System.out.println();
        }
    }

    private static void dfs(int u) {
        dd[u] = 1;
        System.out.print(u+" ");
        for(int i = 0;  i < a[u].size(); i++){
            if(dd[a[u].get(i)]==0)
                dfs(a[u].get(i));
        }
    }
}
