package graph;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class DSA09005 {
    static List<Integer>[] a = new ArrayList[1003];
    static LinkedList<Integer> queue = new LinkedList<>();
    static int[] dd = new int[1003];

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int test = scanner.nextInt();
        while (test-- > 0) {
            int V = scanner.nextInt(), E = scanner.nextInt(), u = scanner.nextInt();
            for (int i = 1; i <= V; i++) {
                a[i] = new ArrayList<>();
                dd[i] = 0;
            }
            for (int i = 1; i <= E; i++) {
                int x = scanner.nextInt();
                int y = scanner.nextInt();
                a[x].add(y);
                a[y].add(x);
            }
            bfs(u);
            System.out.println();
        }
    }

    private static void bfs(int u) {
        queue.add(u);
        dd[u] = 1;
        while (!queue.isEmpty()) {
            int k = queue.poll();
            System.out.print(k + " ");
            for (int i = 0; i < a[k].size(); i++) {
                if (dd[a[k].get(i)] == 0) {
                    queue.add(a[k].get(i));
                    dd[a[k].get(i)] = 1;
                }
            }
        }
    }
}
