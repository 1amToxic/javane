package graph;

import java.util.*;

public class DSA09007 {
    static int V, E, s, t;
    static List<Integer>[] lists = new ArrayList[1003];
    static boolean chuaxet[] = new boolean[1003];
    static int truoc[] = new int[1003];

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int test = scanner.nextInt();
        while (test-- > 0) {
            V = scanner.nextInt();
            E = scanner.nextInt();
            s = scanner.nextInt();
            t = scanner.nextInt();
            for (int i = 0; i <= V; i++) {
                lists[i] = new ArrayList<>();
                chuaxet[i] = true;
                truoc[i] = 0;
            }
            for (int i = 1; i <= E; i++) {
                int a = scanner.nextInt();
                int b = scanner.nextInt();
                lists[a].add(b);
                lists[b].add(a);
            }
            if (bfs()) {
                String sb = "";
                while (t != s) {
                    sb = " "+t + sb;
                    t = truoc[t];
                }
                sb = s+sb;
                System.out.println(sb);
            } else {
                System.out.println(-1);
            }


        }
    }

    private static boolean bfs() {
        Queue<Integer> queue = new LinkedList<>();
        queue.add(s);
        chuaxet[s] = false;
        while (!queue.isEmpty()) {
            int x = queue.poll();
            for (Integer k : lists[x]) {
                if (chuaxet[k]) {
                    truoc[k] = x;
                    chuaxet[k] = false;
                    queue.add(k);
                    if (k == t) return true;
                }
            }
        }
        return false;
    }
}
