package graph;

import java.util.*;

public class DSA09008 {
    static ArrayList<Integer>[] lists = new ArrayList[1005];
    static boolean chuaxet[] = new boolean[1005];
    static int count = 0;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int test = scanner.nextInt();
        while (test-- > 0) {
            count = 0;
            int V = scanner.nextInt(), E = scanner.nextInt();
            for (int i = 0; i <= V; i++) {
                lists[i] = new ArrayList<>();
                chuaxet[i] = true;
            }
            for (int i = 1; i <= E; i++) {
                int x = scanner.nextInt(), y = scanner.nextInt();
                lists[x].add(y);
                lists[y].add(x);
            }
            for (int i = 1; i <= V; i++) {
                if (chuaxet[i]) {
                    bfs(i);
                    count++;
                }
            }
            System.out.println(count);
        }
    }

    private static void bfs(int s) {
        Queue<Integer> queue = new LinkedList<>();
        queue.add(s);
        chuaxet[s] = false;
        while (!queue.isEmpty()) {
            int x = queue.poll();
            for (Integer k : lists[x]) {
                if (chuaxet[k]) {
                    chuaxet[k] = false;
                    queue.add(k);
                }
            }
        }
    }
}
