package graph;

import java.util.*;

public class DSA09010 {
    static List<Integer>[] lists = new ArrayList[1003];
    static boolean chuaxet[] = new boolean[1003];
    static int count = 0;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int test = scanner.nextInt();
        while (test-- > 0) {
            count = 0;
            int V = scanner.nextInt(), E = scanner.nextInt();
            for (int i = 0; i <= V; i++) {
                lists[i] = new ArrayList<>();
                chuaxet[i] = true;
            }
            for (int i = 1; i <= E; i++) {
                int x = scanner.nextInt(), y = scanner.nextInt();
                lists[x].add(y);
            }
            for (int i = 1; i <= V; i++) {
                if (chuaxet[i]) {
                    bfs(i);
                    count++;
                }
            }
            if (count == 1) System.out.println("YES");
            else System.out.println("NO");
        }
    }

    private static void bfs(int s) {
        Queue<Integer> queue = new LinkedList<>();
        queue.add(s);
        chuaxet[s] = false;
        while (!queue.isEmpty()) {
            int x = queue.poll();
            for (Integer k : lists[x]) {
                if (chuaxet[k]) {
                    chuaxet[k] = false;
                    queue.add(k);
                }
            }
        }
    }
}
