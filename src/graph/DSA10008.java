package graph;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class DSA10008 {
    static int[][] dd = new int[1003][1003];
    static boolean[] chuaxet = new boolean[1003];
    static int[] truoc = new int[1003];
    static int[] d = new int[1003];
    static int V, E, s;

    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(System.in);
        int test = scanner.nextInt();
        while (test-- > 0) {
            V = scanner.nextInt();
            E = scanner.nextInt();
            s = scanner.nextInt();
            for (int i = 0; i <= V; i++) {
                for (int j = 0; j <= V; j++) {
                    if (i == j) dd[i][j] = 0;
                    dd[i][j] = (int) 1e9;
                }
            }
            for (int i = 1; i <= E; i++) {
                int u1 = scanner.nextInt(), v = scanner.nextInt(), w = scanner.nextInt();
                dd[u1][v] = w;
            }
            for (int i = 1; i <= V; i++) {
                solve(i);
                System.out.print(d[i]+" ");
            }
            System.out.println();
        }
    }

    private static void solve(int t) {
        int v, u = 0, minp;
        for (v = 1; v <= V; v++) {
            d[v] = dd[s][v];
            truoc[v] = s;
            chuaxet[v] = false;
        }
        truoc[s] = 0;
        d[s] = 0;
        chuaxet[s] = true;
        while (!chuaxet[t]) {
            minp = (int) 1e9;
            for (v = 1; v <= V; v++) {
                if (!chuaxet[v] && (minp > d[v])) {
                    u = v;
                    minp = d[v];
                }
            }
            chuaxet[u] = true;
            if (!chuaxet[t]) {
                for (v = 1; v <= V; v++) {
                    if (!chuaxet[v] && (d[u] + dd[u][v] < d[v])) {
                        d[v] = d[u] + dd[u][v];
                        truoc[v] = u;
                    }
                }
            }
        }
    }
}

