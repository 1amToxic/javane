package graph;

import java.util.*;

public class DSA10011 {
    static int xx[] = new int[]{-1, 1, 0, 0};
    static int yy[] = new int[]{0, 0, -1, 1};
    static int n, m;
    static int[][] dd = new int[505][505];
    static int[][] hh = new int[505][505];
    static boolean[][] check = new boolean[505][505];
    static Map<Integer, Integer> map = new HashMap<>();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int test = scanner.nextInt();
        while (test-- > 0) {
            n = scanner.nextInt();
            m = scanner.nextInt();
            for (int i = 1; i <= n; i++) {
                for (int j = 1; j <= m; j++) {
                    check[i][j] = true;
                    dd[i][j] = scanner.nextInt();
                    hh[i][j] = (int) 1e9;
                }
            }
            bfs();
            System.out.println(hh[n][m]);
        }
    }

    private static void bfs() {
        Queue<Point> queue = new LinkedList<>();
        check[1][1] = false;
        hh[1][1] = dd[1][1];
        queue.add(new Point(1, 1));
        while (!queue.isEmpty()) {
            Point point = queue.poll();
            for (int i = 0; i < 4; i++) {
                int X = point.getX() + xx[i];
                int Y = point.getY() + yy[i];
                if (X >= 1 && Y >= 1 && X <= n && Y <= m) {
                    if (hh[point.getX()][point.getY()] + dd[X][Y] < hh[X][Y]) {
                        hh[X][Y] = hh[point.getX()][point.getY()] + dd[X][Y];
                        queue.add(new Point(X, Y));
                    }
                }
            }
        }
    }
}

class Point {
    int x, y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}