package graph;
import java.util.*;

public class QuayLaiDinh1 {
    static int n, m;
    static ArrayList<Integer> dske[] = new ArrayList[1001];
    static boolean chuaxet[] = new boolean[1001];
    static int truoc[] = new int[1001];

    public static void main(String[] args) {
        Scanner scn = new Scanner(System.in);
        int test = scn.nextInt();
        while (test-- > 0) {
            n = scn.nextInt();
            m = scn.nextInt();
            Set<Integer> set = new HashSet<>();
            for (int i = 0; i <= n; i++) {
                dske[i] = new ArrayList<>();
                chuaxet[i] = true;
                truoc[i] = 0;
            }
            for (int i = 1; i <= m; i++) {
                int u = scn.nextInt();
                int v = scn.nextInt();
                dske[u].add(v);
            }
            bfs(1);
            Stack<Integer> st = new Stack<>();
            st.push(2);
            while (st.peek() != 1) {
                st.push(truoc[st.peek()]);
            }
            while (!st.empty()){
                System.out.println(st.peek());
                set.add(st.pop());
            }
            System.out.println();
            for(int i=1;i<=n;i++){
                chuaxet[i]=true;
                truoc[i]=0;
            }
            bfs(2);
            st.push(1);
            while (st.peek() != 2) {
                st.push(truoc[st.peek()]);
            }
            while (!st.empty()){
                System.out.println(st.peek());
                set.add(st.pop());
            }
            System.out.println(set.size());
        }
    }

    private static void bfs(int i) {
        Queue<Integer> q = new LinkedList<>();
        chuaxet[i] = false;
        q.add(i);
        while (!q.isEmpty()) {
            int x = q.poll();
            for (Integer y : dske[x]) {
                if (chuaxet[y]) {
                    chuaxet[y] = false;
                    truoc[y] = x;
                    q.add(y);
                }
            }
        }
    }
}
