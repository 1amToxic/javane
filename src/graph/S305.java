package graph;

import java.util.*;

public class S305 {
    static List<Integer>[] lists = new ArrayList[1003];
    static int n, m, sum;
    static boolean chuaxet[] = new boolean[1003];
    static int[] dd = new int[1003];

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int test = scanner.nextInt();
        while (test-- > 0) {
            sum = 0;
            n = scanner.nextInt();
            m = scanner.nextInt();
            for (int i = 0; i <= n; i++) {
                lists[i] = new ArrayList<>();
            }
            for (int i = 1; i <= m; i++) {
                int x = scanner.nextInt(), y = scanner.nextInt();
                lists[x].add(y);
            }
            for (int i = 1; i <= n; i++) {
                dd[i] = 0;
                chuaxet[i] = true;
            }
            bfs(1, 2);
            sum += dd[2];
            for (int i = 1; i <= n; i++) {
                dd[i] = 0;
                chuaxet[i] = true;
            }
            bfs(2, 1);
            sum += dd[1];
            System.out.println(sum-2);
        }
    }

    private static void bfs(int s, int t) {
        Queue<Integer> queue = new LinkedList<>();
        queue.add(s);
        chuaxet[s] = false;
        while (!queue.isEmpty()) {
            int x = queue.poll();
            for (Integer y : lists[x]) {
                if (chuaxet[y]) {
                    chuaxet[y] = false;
                    dd[y] = dd[x] + 1;
                    queue.add(y);
                    if (y == t) return;
                }
            }
        }
    }
}
