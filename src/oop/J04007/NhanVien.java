package J04007;

public class NhanVien {
    private String maNhanVien = "00001",hoTen,gioiTinh,ngaySinh,diaChi,maSoThue,ngayKyHopDong;

    public NhanVien(String hoTen, String gioiTinh, String ngaySinh, String diaChi, String maSoThue, String ngayKyHopDong) {
        this.hoTen = hoTen;
        this.gioiTinh = gioiTinh;
        this.ngaySinh = ngaySinh;
        this.diaChi = diaChi;
        this.maSoThue = maSoThue;
        this.ngayKyHopDong = ngayKyHopDong;
    }

    @Override
    public String toString() {
        return maNhanVien+" "+hoTen+" "+gioiTinh+" "+ngaySinh+" "+diaChi+" "+maSoThue+" "+ngayKyHopDong;
    }
}
