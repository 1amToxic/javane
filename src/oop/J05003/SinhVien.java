package J05003;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SinhVien {
    private String hoTen,lop,maSv;
    private Date ngaySinh;
    private float gpa;

    public SinhVien(int id,String hoTen, String lop, String ngaySinh, float gpa) throws ParseException {
        this.hoTen = hoTen;
        this.lop = lop;
        Date d = new Date();
        this.ngaySinh = new SimpleDateFormat("dd/mm/yyyy").parse(ngaySinh);
        this.gpa = gpa;
        this.maSv = "B20DCCN"+String.format("%03d",id);
    }

    @Override
    public String toString() {
        return maSv+" "+hoTen+" "+lop+" "+new SimpleDateFormat("dd/mm/yyyy").format(ngaySinh)+" "+String.format("%.2f",gpa);
    }
}
