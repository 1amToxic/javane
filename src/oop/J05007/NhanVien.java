package J05007;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class NhanVien implements Comparable<NhanVien> {
    private String ma, hoTen, gioiTinh, diaChi, maSoThue;
    private Date ngaySinh, ngayKyHopDong;

    public NhanVien(int id, String hoTen, String gioiTinh, String ngaySinh, String diaChi, String maSoThue, String ngayKyHopDong) throws ParseException {
        this.hoTen = hoTen;
        this.gioiTinh = gioiTinh;
        this.diaChi = diaChi;
        this.maSoThue = maSoThue;
        this.ngaySinh = new SimpleDateFormat("dd/MM/yyyy").parse(ngaySinh);
        this.ngayKyHopDong = new SimpleDateFormat("dd/MM/yyyy").parse(ngayKyHopDong);
        this.ma = String.format("%05d", id);
    }

    @Override
    public String toString() {
        return ma + " " + hoTen + " " + gioiTinh + " " + new SimpleDateFormat("dd/MM/yyyy").format(ngaySinh)
                + " " + diaChi + " " + maSoThue + " " + new SimpleDateFormat("dd/MM/yyyy").format(ngayKyHopDong);
    }

    @Override
    public int compareTo(NhanVien o) {
        return this.ngaySinh.compareTo(o.ngaySinh);
    }
}
