package J05018;

public class HocSinh implements Comparable<HocSinh> {
    private String name;
    private float diemTb;
    private String xepLoai;
    private String stt;

    public HocSinh(int id, String name, float diemTb1) {
        this.name = name;
        diemTb1/=12;
        this.diemTb = (float) Math.round(diemTb1*10)/10;
        this.stt = "HS"+String.format("%02d", id);
        if (diemTb >= 9) {
            xepLoai = "XUAT SAC";
        } else if (diemTb >= 8) {
            xepLoai = "GIOI";
        } else if (diemTb >= 7) {
            xepLoai = "KHA";
        } else if (diemTb >= 5) {
            xepLoai = "TB";
        } else {
            xepLoai = "YEU";
        }
    }

    @Override
    public int compareTo(HocSinh o) {
         if(diemTb == o.diemTb){
             return stt.compareTo(o.stt);
         }else{
             if(diemTb > o.diemTb)  return -1;
             else return 1;
         }
    }

    @Override
    public String toString() {
        return stt+" "+name+" "+diemTb+" "+xepLoai;
    }
}
