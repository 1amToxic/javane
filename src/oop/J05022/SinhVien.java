package J05022;

public class SinhVien {
    private String maSv, hoTen, lop, email;

    public SinhVien(String maSv, String hoTen, String lop, String email) {
        this.maSv = maSv;
        this.hoTen = hoTen;
        this.lop = lop;
        this.email = email;
    }

    public String getLop() {
        return lop;
    }

    @Override
    public String toString() {
        return maSv + " " + hoTen + " " + lop + " " + email;
    }

}
