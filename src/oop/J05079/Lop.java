package J05079;

public class Lop implements Comparable<Lop>{
    private String ma, ten, tenGV, nhomLop;

    public Lop(String ma, String ten, String nhomLop, String tenGV) {
        this.ma = ma;
        this.ten = ten;
        this.tenGV = tenGV;
        this.nhomLop = nhomLop;
    }

    public String getMa() {
        return ma;
    }

    public String getTen() {
        return ten;
    }

    @Override
    public String toString() {
        return nhomLop +" "+tenGV;
    }

    @Override
    public int compareTo(Lop o) {
        return Integer.parseInt(nhomLop)-Integer.parseInt(o.nhomLop);
    }
}
