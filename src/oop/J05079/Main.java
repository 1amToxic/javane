package J05079;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(System.in);
        int test = Integer.parseInt(sc.nextLine());
        List<Lop> list = new ArrayList<>();
        while (test-- > 0) {
            list.add(new Lop(sc.nextLine(), sc.nextLine(), sc.nextLine(), sc.nextLine()));
        }
        int query = Integer.parseInt(sc.nextLine());
        Collections.sort(list);
        while (query-- > 0) {
            String ma = sc.nextLine();
            System.out.print("Danh sach nhom lop mon ");
            for (Lop lop : list) {
                if (lop.getMa().equals(ma)) {
                    System.out.print(lop.getTen()+":");
                    break;
                }
            }
            System.out.println();
            for (Lop lop : list) {
                if (lop.getMa().equals(ma))
                    System.out.println(lop);
            }
        }
    }
}
