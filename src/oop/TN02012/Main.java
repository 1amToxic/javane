package TN02012;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(System.in);
        int test = Integer.parseInt(sc.nextLine());
        Map<String,String> mp = new HashMap<>();
        while (test-->0){
            mp.put(sc.next(),sc.nextLine());
        }
        test = Integer.parseInt(sc.nextLine());
        List<NhanVien> list = new ArrayList<>();
        while (test-->0){
            list.add(new NhanVien(sc.nextLine(),sc.nextLine(),Integer.parseInt(sc.nextLine()),Integer.parseInt(sc.nextLine()),mp));
        }
        for(NhanVien nv : list)
            System.out.println(nv);
    }
}
