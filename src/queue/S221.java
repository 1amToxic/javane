package queue;

import java.io.File;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class S221 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int test = scanner.nextInt();
        while (test-->0){
            int n = scanner.nextInt();
            solve(n);
            System.out.println();
        }
    }

    private static void solve(int n) {
        Queue queue = new LinkedList<String>();
        queue.add("6");
        queue.add("8");
        while (n > 0){
            String a = (String) queue.poll();
            StringBuilder sb = new StringBuilder(a);
            String tmp = a + sb.reverse();
            System.out.print(tmp+" ");
            n--;
            queue.add(a+"6");
            queue.add(a+"8");
        }
    }
}
