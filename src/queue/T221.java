package queue;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class T221 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int test = scanner.nextInt();
        while (test-->0){
            int n = scanner.nextInt();
            solve(n);
        }
    }

    private static void solve(int n) {
        Queue<Long> queue = new LinkedList<>();
        queue.add(1L);
        while (n>0){
            long x = queue.peek();
            queue.poll();
            System.out.print(x+" ");
            queue.add(x*10);
            queue.add(x*10+1);
            n--;
        }
        System.out.println();
    }
}
