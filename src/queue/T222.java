package queue;


import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class T222 {
    public static void main(String[] args)  {
        Scanner scanner = new Scanner(System.in);
        int test = scanner.nextInt();
        while (test-- > 0) {
            long n = scanner.nextLong();
            solve(String.valueOf(n));
        }
    }

    private static void solve(String n) {
        Queue queue = new LinkedList<String>();
        queue.add("1");
        long dem = 0;
        while (!queue.isEmpty()) {
            String k = (String) queue.peek();
            queue.poll();
            if (k.length() < n.length()) {
                dem++;
                queue.add(k + "0");
                queue.add(k + "1");

            }
            if(n.length() == k.length() && k.compareTo(n)<=0)  dem++;
        }
        System.out.println(dem);
    }
}
