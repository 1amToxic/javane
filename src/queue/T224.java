package queue;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class T224 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(System.in);
        int test = scanner.nextInt();
        while (test-->0){
            int n = scanner.nextInt();
            System.out.println(solve(n));
        }
    }

    private static String solve(int n) {
        Queue queue = new LinkedList<String>();
        queue.add("9");
        if(9%n==0)  return "9";
        while (!queue.isEmpty()){
            String a = (String) queue.poll();
            if(Long.parseLong(a+"0") % n ==0)   return a+"0";
            if(Long.parseLong(a+"9") % n ==0)   return a+"9";
            queue.add(a+"0");
            queue.add(a+"9");
        }
        return "";
    }
}
