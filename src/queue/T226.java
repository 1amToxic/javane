package queue;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class T226 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int test = scanner.nextInt();
        while (test-- > 0) {
            int s = scanner.nextInt();
            int t = scanner.nextInt();
            System.out.println(solve(s, t));
        }
    }

    private static int solve(int s, int t) {
        Queue queue = new LinkedList<String>();
        int Max = Math.max(s,t);
        if(t == s) return 0;
        int[] dd = new int[2*Max+1];
        queue.add(String.valueOf(s));
        while (!queue.isEmpty()) {
            int a = Integer.parseInt((String) queue.poll());
            if (a > t) {
                if(dd[a - 1] == 0) {
                    queue.add(String.valueOf(a - 1));
                    dd[a - 1] = dd[a] + 1;
                }
            } else if (a < t) {
                if (dd[a - 1] == 0 && a - 1 > 0) {
                    queue.add(String.valueOf(a - 1));
                    dd[a - 1] = dd[a] + 1;
                }
                if (dd[a * 2] == 0) {
                    dd[a * 2] = dd[a] + 1;
                    queue.add(String.valueOf(a * 2));
                }
            } else return dd[t];
        }
        return 0;
    }
}
