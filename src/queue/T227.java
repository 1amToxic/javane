package queue;

import java.util.*;

public class T227 {
    static int[] dd = new int[10000];
    static List<Integer> listSNT = new ArrayList<>();
    static Map<Integer, List<Integer>> mapNext = new HashMap<>();
    static int[] count = new int[10000];

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int test = scanner.nextInt();
        sang();
        init();
        while (test-- > 0) {
            for (int i = 1000; i <= 9999; i++)
                count[i] = 0;
            int s = scanner.nextInt();
            int t = scanner.nextInt();
            System.out.println(solve(s, t));
        }
    }

    private static int solve(int s, int t) {
        Queue queue = new LinkedList<String>();
        queue.add(String.valueOf(s));
        while (!queue.isEmpty()) {
            String a = (String) queue.poll();
            if (Integer.parseInt(a) == t) {
                return count[t];
            } else {
                List<Integer> list = mapNext.get(Integer.parseInt(a));
                for (int i = 0; i < list.size(); i++) {
                    if (count[list.get(i)] == 0) {
                        count[list.get(i)] = count[Integer.parseInt(a)] + 1;
                        queue.add(String.valueOf(list.get(i)));
                    }
                }
            }
        }
        return 0;
    }

    private static void init() {

        for (int i = 0; i < listSNT.size(); i++)
            mapNext.put(listSNT.get(i), new ArrayList<>());
        for (int i = 0; i < listSNT.size(); i++) {
            for (int j = i + 1; j < listSNT.size(); j++) {
                if (checkDiff(listSNT.get(i), listSNT.get(j))) {
                    List<Integer> list = mapNext.get(listSNT.get(i));
                    list.add(listSNT.get(j));
                    mapNext.put(listSNT.get(i), list);
                    list = mapNext.get(listSNT.get(j));
                    list.add(listSNT.get(i));
                    mapNext.put(listSNT.get(j), list);
                }
            }
        }
    }

    private static void sang() {
        for (int i = 2; i <= 9999; i++) {
            if (dd[i] == 0) {
                for (int j = i * i; j <= 9999; j += i)
                    dd[j] = 1;
            }
        }
        for (int i = 1000; i <= 9999; i++)
            if (dd[i] == 0) listSNT.add(i);
    }

    private static boolean checkDiff(int a, int b) {
        int d = 0;
        while (a > 0) {
            if (a % 10 != b % 10) d++;
            a /= 10;
            b /= 10;
        }
        return d == 1;
    }
}
