package stack;


import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Stack;

public class DSA07019 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(System.in);
        int test = scanner.nextInt();
        while (test-->0){
            int n = scanner.nextInt();
            int h[] = new int[n+5];
            for(int i = 1; i <= n; i++)
                h[i] = scanner.nextInt();
            solve(n,h);
        }
    }

    private static void solve(int n, int[] h) {
        long[] right = new long[n+5];
        long[] left = new long[n+5];
        h[0] = 0; h[n+1] = 0;
        Stack<Integer> stack = new Stack<>();
        Stack<Integer> stack1 = new Stack<>();
        //Left
        for(int i = 1; i <= n; i++){
            while (!stack.empty() && h[i] <= h[stack.peek()]) stack.pop();
            if (stack.empty()) left[i] = 0;
            else left[i] = stack.peek();
            stack.push(i);
        }
        //Right
        for(int i = n; i >= 1; i--){
            while (!stack1.empty() && h[i] <= h[stack1.peek()]) stack1.pop();
            if (stack1.empty()) right[i] = n+1;
            else right[i] = stack1.peek();
            stack1.push(i);
        }
        long Max = 0;
        for(int i = 1; i <= n; i++) {
            Max = Math.max(Max, h[i] * (right[i] - left[i] - 1));
        }
        System.out.println(Max);;
    }
}
