package stack;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Stack;

public class DSA07020 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        while (t-->0){
            Stack<Character> stack = new Stack<>();
            String s = sc.nextLine();
            for(int i = 0; i < s.length(); i++){
                if(s.charAt(i) == ' '){
                    while (!stack.empty()){
                        System.out.print(stack.peek());stack.pop();
                    }
                    System.out.print(" ");
                }else{
                    stack.push(s.charAt(i));
                }
            }
            while (!stack.empty()){
                System.out.print(stack.peek());stack.pop();
            }
            System.out.println();
        }
    }
}
