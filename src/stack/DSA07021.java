package stack;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Stack;

public class DSA07021 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("D:\\Documents\\TN\\javane\\src\\stack\\input.txt"));
        int test = Integer.parseInt(scanner.nextLine());
        while (test-- > 0) {
            String s = scanner.nextLine();
            System.out.println(solve(s));
        }
    }

    private static int solve(String s) {
        int Max = 0;
        Stack<Integer> stack = new Stack<>();
        stack.push(-1);
        for (int i = 0; i < s.length(); i++) {
            if(s.charAt(i) == '(') stack.push(i);
            else{
                stack.pop();
                if(stack.size() > 0)    Max = Math.max(Max,i-stack.peek());
                if(stack.size() == 0)   stack.push(i);
            }
        }
        return Max;
    }
}
