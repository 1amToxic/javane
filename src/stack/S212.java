package stack;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Stack;

public class S212 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(System.in);
        int test = Integer.parseInt(scanner.nextLine());
        while (test-- > 0) {
            String s = scanner.nextLine();
            int number = 1;
            Stack<Integer> stack = new Stack<>();
            for (int i = 0; i < s.length(); i++) {
                if(s.charAt(i) == '('){
                    System.out.print(number+" ");
                    stack.push(number++);
                }else if(s.charAt(i) == ')'){
                    int a = stack.peek();
                    stack.pop();
                    System.out.print(a+" ");
                }
            }
            System.out.println();
        }
    }
}
