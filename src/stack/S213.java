package stack;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
import java.util.Stack;

public class S213 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(System.in);
        int test = scanner.nextInt();
        while (test-- > 0) {
            int n = scanner.nextInt();
            Queue<String> queue = new LinkedList<>();
            Stack<String> stack = new Stack<>();
            stack.push("6");
            stack.push("8");
            queue.add("6");
            queue.add("8");
            while (!queue.isEmpty()){
                String s = queue.poll();

                if(s.length() == n){
                    break;
                }
                queue.add(s+"6");
                queue.add(s+"8");
                stack.push(s+"6");
                stack.push(s+"8");
            }
            System.out.println(stack.size());
            while (!stack.isEmpty()){
                System.out.print(stack.pop()+" ");
            }
            System.out.println();
        }
    }
}
