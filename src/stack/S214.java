package stack;

import java.util.Scanner;
import java.util.Stack;

public class S214 {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        int test = scanner.nextInt();
        while (test-- > 0) {
            int n = scanner.nextInt();
            int[] a = new int[n + 5];
            int[] right = new int[n + 5];
            Stack<Integer> stack = new Stack<>();
            for (int i = 1; i <= n; i++) {
                a[i] = scanner.nextInt();
            }
            for (int i = 1; i <= n; i++) {
                while (!stack.isEmpty() && a[i] >= a[stack.peek()]) stack.pop();
                if (stack.isEmpty()) right[i] = 0;
                else right[i] = stack.peek();
                stack.push(i);
            }
            for (int i = 1; i <= n; i++) {
                System.out.print(i - right[i] + " ");
            }
            System.out.println();
        }
    }
}
