package stack;

import java.util.Scanner;
import java.util.Stack;

public class S216 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        Stack<Character> stack = new Stack<>();
        Stack<Character> stack1 = new Stack<>();
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '>') {
                if(!stack1.isEmpty()){
                    char a = stack1.pop();
                    stack.push(a);
                }
            } else if (s.charAt(i) == '<') {
                if(!stack.isEmpty()){
                    char a = stack.pop();
                    stack1.push(a);
                }
            } else if (s.charAt(i) == '-') {
                if(!stack.isEmpty()) stack.pop();
            } else stack.push(s.charAt(i));
        }
        String s1 = "";
        while (!stack1.isEmpty()){
            stack.push(stack1.pop());
        }
        while (!stack.isEmpty()){
            s1=stack.pop()+s1;
        }
        System.out.println(s1);
    }
}
