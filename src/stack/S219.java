package stack;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Stack;

public class S219 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(System.in);
        int test = Integer.parseInt(scanner.nextLine());
        while (test-->0){
            String s = scanner.nextLine();
            if(check(s)) System.out.println("YES");
            else System.out.println("NO");
        }
    }

    private static boolean check(String s) {
        Stack<Character> stack = new Stack<>();
        for(int i = 0; i < s.length(); i++){
            if(s.charAt(i) =='(' || s.charAt(i) =='['){
                stack.push(s.charAt(i));
            }else if(s.charAt(i) == ')' || s.charAt(i) == ']'){
                if(!stack.isEmpty()) {
                    if (s.charAt(i) == ')') {
                        if(stack.peek() == '(') stack.pop();
                        else return false;
                    }
                    if (s.charAt(i) == ']') {
                        if(stack.peek() == '[') stack.pop();
                        else return false;
                    }
                }else return false;
            }
        }
        return stack.isEmpty();
    }
}
