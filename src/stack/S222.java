package stack;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Stack;

public class S222 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();
        int[] a = new int[n + 5];
        int[] b = new int[n + 5];
        for (int i = 1; i <= n; i++) {
            a[i] = scanner.nextInt();
            b[i] = m - a[i];
        }
        int[] right = new int[n + 5];
        int[] right1 = new int[n + 5];
        int[] left = new int[n + 5];
        int[] left1 = new int[n + 5];
        Stack<Integer> stack = new Stack<>();
        Stack<Integer> stack1 = new Stack<>();
        for (int i = n; i >= 1; i--) {
            while (!stack.isEmpty() && a[i] <= a[stack.peek()]) stack.pop();
            if (stack.isEmpty()) {
                right[i] = n + 1;

            } else right[i] = stack.peek();
            stack.push(i);
            while (!stack1.isEmpty() && b[i] <= b[stack1.peek()]) stack1.pop();
            if (stack1.isEmpty()) right1[i] = n + 1;
            else right1[i] = stack1.peek();
            stack1.push(i);
        }
        stack.clear();
        stack1.clear();
        for (int i = 1; i <= n; i++) {
            while (!stack.isEmpty() && a[i] <= a[stack.peek()]) stack.pop();
            if (stack.isEmpty()) left[i] = 0;
            else left[i] = stack.peek();
            stack.push(i);
            while (!stack1.isEmpty() && b[i] <= b[stack1.peek()]) stack1.pop();
            if (stack1.isEmpty()) left1[i] = 0;
            else left1[i] = stack1.peek();
            stack1.push(i);
        }
        int Max = 0;
        for (int i = 1; i <= n; i++) {
            Max = Math.max(Max, a[i] * (right[i] - left[i] - 1));
            Max = Math.max(Max, b[i] * (right1[i] - left1[i] - 1));
        }
        System.out.println(Max);
    }
}
