package stack;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Stack;

public class S226 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(System.in);
        int test = Integer.parseInt(scanner.nextLine());
        while (test-- > 0) {
            String s = scanner.nextLine();
            System.out.println(check(s));
        }
    }

    private static int check(String s) {
        Stack<Integer> stack = new Stack<>();
        int Max = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '(') {
                stack.push(stack.size()+1);
                Max = Math.max(stack.size(),Max);
            } else if(s.charAt(i) == ')'){
                if (stack.isEmpty()) return -1;
                stack.pop();
            }
        }
        if(!stack.isEmpty())    return -1;
        return Max;
    }
}
