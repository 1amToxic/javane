package stack;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Stack;

public class T212 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("D:\\Documents\\TN\\javane\\src\\stack\\input.txt"));
        int test = Integer.parseInt(scanner.nextLine());
        while (test-- > 0) {
            String s = scanner.nextLine();
            if (check(s)) System.out.println("Yes");
            else System.out.println("No");
        }
    }

    private static boolean check(String s) {
        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '(') stack.push(i);
            else if (s.charAt(i) == ')') {
                int j = stack.peek();
                stack.pop();
                if (i - j == 2) {
                    System.out.println("1");
                    return true;
                }
                if (s.charAt(j + 1) == '(' && s.charAt(i - 1) == ')') {
                    System.out.println("2");
                    return true;
                }
            }
        }
        return false;
    }
}
