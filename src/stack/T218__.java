package stack;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Stack;

public class T218__ {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(System.in);
        int test = scanner.nextInt();
        while (test-- > 0) {
            int n = scanner.nextInt(), a[] = new int[n+5];
            for (int i = 0; i < n; i++)
                a[i] = scanner.nextInt();
            xuly(a, n);
        }
    }

    private static void xuly(int[] a, int n) {
        int right[] = new int[n], i;
        Stack<Integer> stack = new Stack<>();
        for (i = n-1; i >=0; i--) {
            while (!stack.isEmpty() && a[i] >= a[stack.peek()]) stack.pop();
            if(stack.isEmpty()) right[i] = -1;
            else right[i] =  a[stack.peek()];
            stack.push(i);
        }
        for (i = 0; i < n; i++)
            System.out.print(right[i] + " ");
        System.out.println();
    }
}
