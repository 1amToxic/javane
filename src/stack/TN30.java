package stack;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Stack;

public class TN30 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("D:\\Documents\\TN\\javane\\src\\queue\\input.txt"));
        int test = Integer.parseInt(sc.nextLine());
        while (test-- > 0) {
            String s = sc.nextLine();
            Stack<Character> stack = new Stack<>();
            int result = 0;
            //    ][ : 0
            //    [] : 1
            for (int i = 0; i < s.length(); i++) {
                if (s.charAt(i) == ']') {
                    if (!stack.isEmpty() && stack.peek() == '[') stack.pop();
                    else{
                        stack.push(s.charAt(i));
                    }
                } else {
                    if (!stack.isEmpty() && stack.peek() == ']') {
                        result++;
                        stack.pop();
                        if (!stack.isEmpty()) {
                            stack.push('[');
                            stack.push(']');
                        }
                    } else stack.push(s.charAt(i));
                }
            }
            while (!stack.empty()) {
                char a = stack.peek();
                stack.pop();
                char b = stack.peek();
                stack.pop();
                if (a == ']' && b == '[') result++;
            }
            System.out.println(result);
        }
    }
}
